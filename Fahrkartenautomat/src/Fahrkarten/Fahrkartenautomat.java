﻿package Fahrkarten;

import java.util.Scanner;

public class Fahrkartenautomat {		
	
	public static void amountAndPrice(Scanner key) {

	       double price;
	       int amount = 0;
	       
	       //der Preis?
	       System.out.print("Preis?: ");
	       price = key.nextDouble();
	       //Wenn kein Preis angeben wird oder der Preis kleiner als 0 ist (Extra funktion - Nicht teil der aufgabe)
	       if (price <= 0.0) {
	    	   price = 1.0;
	       }
	      
	       //wie viele?
	       System.out.print("Wie viele?: ");
	       
	       amount = key.nextInt();
	       calculate(amount, price, key);
	       
	}
		


	public static void calculate(int amount, double price, Scanner key) {
		 double zuZahlenderBetrag; 
	     double eingezahlterGesamtbetrag= 0.0;
	     double eingeworfeneMünze;
	     double rückgabebetrag;
	     
		 zuZahlenderBetrag = amount * price;

	       // Geldeinwurf
	       // -----------
	     
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	   double euro = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	           
	           System.out.printf("Noch zu Zahlen: %.2f", euro).print(" Euro \n");
	           System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	           eingeworfeneMünze = key.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       
	       }

	       // Fahrscheinausgabe 
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
	            Thread.sleep(250);
	        } catch (InterruptedException e) {
	            // TODO Auto-generated catch block 
	            e.printStackTrace();
	        }
	       }
	       System.out.println("\n\n");

	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       backMoney(rückgabebetrag);
	}
	       
	       public static void backMoney(double rückgabebetrag) {
	       if(rückgabebetrag > 0.0)
	       {
	           System.out.printf("Der Rückgabebetrag in Höhe von %.2f", rückgabebetrag).print(" Euro \n");

	           System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	              System.out.println("2 EURO");
	              rückgabebetrag -= 2.0;
	              // rückgabebetrag = rückgabebetrag - 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	              System.out.println("1 EURO");
	              rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	              System.out.println("50 CENT");
	              rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	              System.out.println("20 CENT");
	               rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	              System.out.println("10 CENT");
	              rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	              System.out.println("5 CENT");
	               rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }
	
	       
	     public static void runAllMethods(Scanner key) {
	    	  amountAndPrice(key); 
	     }

}
